# FASER - IFT_Emulsion Track Matching

This repo contains analysis of reconstructed tracks data for the IFT and Emulsion detector. 

## Emulsion Data
you will there are 4 files which contain reconstructed track data done by Tomoko. format.txt gives the format of each files.

Original files can be found here: /eos/project-f/fasernu-pilot-run/Data/IFT_test_2021/trk_data/ 

emulsion_recon.ipynb performs analysis of the 4 files


## IFT Data
the jupyternbook contained here performs analysis of IFT data. 
files needed to perform the analysis can be found here:
/eos/project/f/faser-commissioning/reco/Commissioning2021/r0005/

root file with position of each spacepoint in a track segment from a line fit is used in the jupyternbook for analysis. find it here:
/afs/cern.ch/user/k/keli/work/public/ForVan/trackerspfit.root

sp_track_x/y/z are the 3d positions, track_p0/p1/p2/p3 are the 4 parameters (line defines as x=p0 + p1*z, y = p2+p3*z), each track segment has 3 spacepoint, so in this file, the first 3 spacepoint come from the same track, the second 3 come from another track..., 
(provided by Ke Li)




