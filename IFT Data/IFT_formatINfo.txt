hi @Van, the root file with positions of each spacepoint in a track segment from a 
line fit is here: /afs/cern.ch/user/k/keli/work/public/ForVan/trackerspfit.root

sp_track_x/y/z are the 3d positions, track_p0/p1/p2/p3 are the 4 parameters (line defines as x=p0 + p1*z, y = p1+p2*z), each track segment has 3 spacepoint, so in this file, the first 3 spacepoint come from the same track, the second 3 come from another track..., 
please let me know if you have any questions


6:50 PM
Hi @ke you wrote  (line defines as x=p0 + p1*z, y = p1+p2*z): Is this a typo? Should it be (line defines as x=p0 + p1*z, y = p2+p3*z) and 
p0 -> x0
p2 -> y0

p1 -> tan(thetax)
p3 -> tan(thetay)

(->) meaning "corresponds to"





translation of x,y

and maybe rotation in phi

make sure Emulsion tx,ty are pointing in the same direction (to IFT)


TODO: Figure out transformation
	Emulsion extrapolate to 4cm
	
then do translation of that x,y,z

we may need to flip tx ty

